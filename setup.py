"""Setup package."""
import os
from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.md')) as fd:
    long_description = fd.read()


setup(
    name='datadefinition',
    version='0.0.1',
    description='CKI pipeline rc file data definition.',
    long_description=long_description,
    url='https://gitlab.com/cki-project/datadefinition',
    author='CKI Team',
    author_email='cki-project@redhat.com',
    packages=find_packages(),
    install_requires=open('requirements.txt').read().splitlines(),
    python_requires='>=3',
    package_data={
        '': ['*.yaml', '*.txt']
    }
)
