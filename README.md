# datadefiniton

CKI pipeline rc file serialization

## Maintainers

* [jracek](https://gitlab.com/jracek)
* 1 rack free!

## Design principles

This repository hold code that is used within other CKI Project applications,
including:

* pipeline-definition
* pipeline-stats
* data warehouse

## Developer guidelines

**Use care when making changes to this repository.** Many other applications
depend on this code to work properly.

## Development

Run the scripts found in [.gitlab-ci.yml] to test your changes before
submitting the merge request.

[.gitlab-ci.yml]: https://gitlab.com/cki-project/datadefinition/blob/master/.gitlab-ci.yml
